package parser;

import java.util.Objects;

public class Key {

    private String name;
    private Quotation quote;

    public Key(String name, Quotation quote) {
        this.name = name;
        this.quote = quote;
    }

    public static Key getKeyFromString(String string) {
        Objects.requireNonNull(string, "Нельзя извлечь ключ из null");
        if (string.trim().isEmpty()) {
            throw new IllegalArgumentException("Нельзя извлечь ключ из пустой строки");
        }
        char first = string.charAt(0);
        if (first == '\"') {
            String name = string.substring(1, string.length() - 1);
            return new Key(name, Quotation.DOUBLE);
        } else if (first == '\'') {
            String name = string.substring(1, string.length() - 1);
            return new Key(name, Quotation.SINGLE);
        } else {
            return new Key(string, Quotation.EMPTY);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Quotation getQuote() {
        return quote;
    }

    public void setQuote(Quotation quote) {
        this.quote = quote;
    }

    @Override
    public String toString() {
        return quote.getRepresentation() + name + quote.getRepresentation();
    }
}
