package parser;

import org.apache.commons.lang3.StringUtils;
import parser.sources.Source;
import parser.value.Value;
import parser.value.containers.JsonObject;
import parser.value.primitives.JsonString;

import static parser.Key.getKeyFromString;

public class JsonParser {

    public Document parse(Source source) {
        String resourceAsString = source.substring(0, source.length());

        int jsonOpenIndex = StringUtils.indexOf(resourceAsString, "{");
        int jsonCloseIndex = StringUtils.lastIndexOf(resourceAsString, "}");

        String prefix = StringUtils.substringBefore(resourceAsString, "{");
        String suffix = StringUtils.substringAfterLast(resourceAsString, "}");

        String json = resourceAsString.substring(jsonOpenIndex + 1, jsonCloseIndex);
        JsonObject root = parseObject(json);
        return new Document(prefix, suffix, root);
    }

    private JsonObject parseObject(String json) {
        String[] tokens = json.split(",");
        JsonObject jsonObject = new JsonObject();
        for (String tokenAsString : tokens) {
            Token token = parserToken(tokenAsString);
            jsonObject.addToken(token);
        }
        return jsonObject;
    }

    private Value parseString(String data) {
        if (data.charAt(0) != '\"') {
            throw new IllegalArgumentException("Malformed String. Should start with [\"]");
        }
        String value = data.substring(1, data.length() - 1);
        return new JsonString(value);
    }

    private Token parserToken(String tokenData) {
        String beforeColon = StringUtils.substringBefore(tokenData, ":");
        ContentWithWhitespaces keyWithSpaces = new ContentWithWhitespaces(beforeColon);
        String keyContent = keyWithSpaces.getData();
        String keyPrefix = keyWithSpaces.getPrefix();
        String keySuffix = keyWithSpaces.getSuffix();
        Key key = getKeyFromString(keyContent);

        String afterColon = StringUtils.substringAfter(tokenData, ":");
        ContentWithWhitespaces valueWithSpaces = new ContentWithWhitespaces(afterColon);
        String valueContent = valueWithSpaces.getData();
        String valuePrefix = valueWithSpaces.getPrefix();
        String valueSuffix = valueWithSpaces.getSuffix();
        Value value = parseString(valueContent);

        Token token = new Token();
        token.setPrefix(keyPrefix);
        token.setSuffix(valueSuffix);
        token.setKey(key);
        token.setSeparator(keySuffix + ":" + valuePrefix);
        token.setValue(value);

        return token;
    }
}
