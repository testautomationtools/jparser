package parser.value;

public enum Type {
    OBJECT,
    ARRAY,
    STRING,
    NUMBER
}
