package parser.value;

public interface Value {

    Type getType();
}
