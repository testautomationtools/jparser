package parser.value.containers;

import parser.Token;
import parser.value.Type;

import java.util.ArrayList;
import java.util.List;

public class JsonObject implements Container {

    private static final String OPENING = "{";
    private static final String CLOSING = "}";
    private List<Token> tokens;

    public JsonObject() {
        tokens = new ArrayList<>();
    }

    public void addToken(Token token) {
        tokens.add(token);
    }

    @Override
    public List<Token> getChildren() {
        return tokens;
    }

    @Override
    public Type getType() {
        return Type.OBJECT;
    }

    @Override
    public String toString() {
        if (tokens.isEmpty()) {
            return OPENING + CLOSING;
        } else if (tokens.size() == 1) {
            return OPENING + tokens.get(0) + CLOSING;
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(OPENING);
            int lastIndex = tokens.size() - 1;
            for (int i = 0; i < lastIndex; i++) {
                sb.append(tokens.get(i).toString())
                        .append(",");
            }
            sb.append(tokens.get(lastIndex));
            sb.append(CLOSING);
            return sb.toString();
        }
    }
}
