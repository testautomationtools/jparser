package parser.value.containers;

import parser.Token;
import parser.value.Value;

import java.util.List;

public interface Container extends Value {

    List<Token> getChildren();

}
