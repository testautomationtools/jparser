package parser.value.primitives;

import parser.value.Type;

import static parser.Quotation.DOUBLE;

public class JsonString implements Primitive {

    private String value;

    public JsonString(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public Type getType() {
        return Type.STRING;
    }

    @Override
    public String toString() {
        return DOUBLE.getRepresentation() + value + DOUBLE.getRepresentation();
    }
}
