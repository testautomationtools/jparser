package parser.validation;

public class CharValidator {
    public boolean isWhitespace(char c) {
        switch (c) {
            case '\t':
            case '\n':
            case '\r':
            case ' ':
                return true;
            default:
                return false;
        }
    }
}
