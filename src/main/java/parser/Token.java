package parser;

import parser.value.Type;
import parser.value.Value;

public class Token {

    private String prefix;
    private String suffix;
    private Key key;
    private String separator;
    private Value value;

    public Token() {
        this.prefix = "";
        this.suffix = "";
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public Type getType() {
        return value.getType();
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public String toString() {
        return prefix + key.toString() + separator + value.toString() + suffix;
    }
}
