package parser;

import org.apache.commons.lang3.StringUtils;

public class ContentWithWhitespaces {
    private String prefix;
    private String suffix;
    private String data;

    public ContentWithWhitespaces(String data) {
        this.data = data.trim();
        prefix = StringUtils.substringBefore(data, this.data);
        suffix = StringUtils.substringAfter(data, this.data);
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getData() {
        return data;
    }
}
