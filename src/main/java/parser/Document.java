package parser;

import parser.value.containers.JsonObject;

public class Document {

    private String prefix;
    private String suffix;
    private JsonObject rootObject;

    Document(String prefix, String suffix, JsonObject rootObject) {
        this.prefix = prefix;
        this.suffix = suffix;
        this.rootObject = rootObject;
    }

    public JsonObject getRootObject() {
        return rootObject;
    }

    @Override
    public String toString() {
        return prefix + rootObject.toString() + suffix;
    }
}
