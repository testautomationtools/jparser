package parser;

public enum Quotation {
    DOUBLE("\""),
    SINGLE("'"),
    EMPTY("");

    private String representation;

    Quotation(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
