package parser.sources;

public interface Source {

    int length();

    char charAt(int index);

    String substring(int beginIndex, int endIndex);

}
