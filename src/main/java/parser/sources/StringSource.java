package parser.sources;

public class StringSource implements Source {

    private String data;

    public StringSource(String data) {
        this.data = data;
    }

    @Override
    public int length() {
        return data.length();
    }

    @Override
    public char charAt(int index) {
        return data.charAt(index);
    }

    @Override
    public String substring(int beginIndex, int endIndex) {
        return data.substring(beginIndex, endIndex);
    }
}
