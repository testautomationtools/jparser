package dummy;

import parser.value.Type;
import parser.value.Value;

public class Dummy implements Value {

    @Override
    public Type getType() {
        return null;
    }

    @Override
    public String toString() {
        return "Dummy";
    }
}
