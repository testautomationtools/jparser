package integration;

import org.junit.jupiter.api.Test;
import parser.Document;
import parser.JsonParser;
import parser.sources.Source;
import parser.sources.StringSource;
import parser.value.containers.JsonObject;

import static org.assertj.core.api.Java6Assertions.assertThat;

class Story {

    @Test
    void doIT() {
        String json = "{key : \"value\"}";
        Source source = new StringSource(json);
        JsonParser parser = new JsonParser();
        Document document = parser.parse(source);
        JsonObject jsonObject = document.getRootObject();

        assertThat(jsonObject.toString())
                .isEqualTo(json);
    }
}
