package parser;

import dummy.Dummy;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Java6Assertions.assertThat;

@DisplayName("Token")
class TokenSpec {

    @DisplayName("Правильно выводит текстовое представление, при разных типах выделения ключа")
    @ParameterizedTest()
    @MethodSource("checkQuotationTypes")
    void checkTokenToStringWithDifferentQuotations(String expected, Quotation quotation) {
        Token token = new Token();
        token.setKey(new Key("key", quotation));
        token.setSeparator(":");
        token.setValue(new Dummy());
        String actual = token.toString();
        assertThat(actual)
                .isEqualTo(expected);
    }

    private static Stream<Arguments> checkQuotationTypes() {
        return Stream.of(
                Arguments.of("\"key\":Dummy", Quotation.DOUBLE),
                Arguments.of("'key':Dummy", Quotation.SINGLE),
                Arguments.of("key:Dummy", Quotation.EMPTY));
    }
}
