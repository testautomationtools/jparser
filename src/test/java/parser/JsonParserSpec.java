package parser;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import parser.sources.Source;
import parser.sources.StringSource;
import parser.value.Type;
import parser.value.containers.JsonObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.assertj.core.api.Java6Assertions.assertThat;

@DisplayName("JsonParser")
class JsonParserSpec {

    private static final Logger LOG = LoggerFactory.getLogger(JsonParserSpec.class);

    @DisplayName("Из json строки может быть распарсен JsonObject")
    @Test
    void canFindStringToken() {
        String jsonAsString = "{key : \"value\"}";
        JsonParser parser = new JsonParser();
        Document document = parser.parse(new StringSource(jsonAsString));
        JsonObject json = document.getRootObject();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Type.OBJECT, json.getType()),
                () -> Assertions.assertDoesNotThrow(json::getChildren)
        );
    }

    @DisplayName("Проверка строкового предстваления единственного токена при парсинге")
    @Test
    void stringRepresentationOfOneToken() {
        String input = "{key:\"value\"}";
        String expected = "key:\"value\"";
        Token token = getSingleToken(input);
        String actual = token.toString();
        Assertions.assertEquals(expected, actual);
    }

    private Token getSingleToken(String jsonAsString) {
        JsonParser parser = new JsonParser();
        Document document = parser.parse(new StringSource(jsonAsString));
        JsonObject json = document.getRootObject();
        return json.getChildren().get(0);
    }

    @DisplayName("Json состоит из двух токенов")
    @Nested
    class JsonObjectOfTwoTokens {

        private JsonParser parser;
        private Source json;
        private String inputStringJson;

        @BeforeEach
        void setUp() {
            parser = new JsonParser();
            String path = "src/test/resources/twoTokens.json";
            inputStringJson = readFileContent(path);
            json = new StringSource(inputStringJson);
        }

        @DisplayName("Распаршеный Json содержит два токена")
        @Test
        void containsTwoTokens() {
            Document document = parser.parse(json);
            int size = document.getRootObject().getChildren().size();
            assertThat(size).isEqualTo(2);
        }

        @DisplayName("Первый token полученного json равен указанной строке")
        @Test
        void checkFirstToken() {
            String tempResult = StringUtils.substringAfter(inputStringJson, "{");
            String expectedFirstToken = StringUtils.substringBefore(tempResult, ",");

            Document document = parser.parse(json);
            Token token = document.getRootObject().getChildren().get(0);
            String actual = token.toString();
            Assertions.assertEquals(expectedFirstToken, actual);
        }

        @DisplayName("Второй token полученного json равен указанной строке")
        @Test
        void checkSecondToken() {
            String tempResult = StringUtils.substringAfter(inputStringJson, ",");
            String expectedFirstToken = StringUtils.substringBefore(tempResult, "}");

            Document document = parser.parse(json);
            Token token = document.getRootObject().getChildren().get(1);
            String actual = token.toString();
            Assertions.assertEquals(expectedFirstToken, actual);
        }

        @DisplayName("Строковое представление document равно исходному json")
        @Test
        void jsonStringRepresentationEqualsParsedString() {
            Document document = parser.parse(json);
            String actual = document.toString();
            assertThat(actual).isEqualTo(inputStringJson);
        }
    }

    @DisplayName("Json состоит из трех примитивных токенов")
    @Nested
    class JsonObjectOfThreeTokens {

        private JsonParser parser;
        private Source json;
        private String inputStringJson;

        @BeforeEach
        void setUp() {
            parser = new JsonParser();
            String path = "src/test/resources/threeTokens.json";
            inputStringJson = readFileContent(path);
            json = new StringSource(inputStringJson);
        }

        @DisplayName("Распаршеный Json содержит три токена")
        @Test
        void containsTwoTokens() {
            Document document = parser.parse(json);
            int size = document.getRootObject().getChildren().size();
            assertThat(size).isEqualTo(3);
        }

        @DisplayName("Строковое представление document равно исходному json")
        @Test
        void jsonStringRepresentationEqualsParsedString() {
            Document document = parser.parse(json);
            String actual = document.toString();
            assertThat(actual).isEqualTo(inputStringJson);
        }
    }

    private String readFileContent(String path) {
        String fileContent = "";
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(path));
            fileContent = new String(bytes);
        } catch (IOException e) {
            LOG.error("Не удалось получить содержимое файла по относительному пути [{}]", path, e);
        }
        return fileContent;
    }
}
