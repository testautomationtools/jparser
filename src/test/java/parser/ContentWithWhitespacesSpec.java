package parser;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ContentWithWhitespacesSpec {

    private static final String MESSAGE = "не соответствует ожиданемому";

    @DisplayName("Префикс и Суфикс должны быть пустой строкой")
    @Test
    void prefixAndSuffixShouldBeEmptyString() {
        String prefix = "";
        String data = "key";
        String suffix = "";
        ContentWithWhitespaces contentWithWhitespaces = new ContentWithWhitespaces(prefix + data + suffix);
        checkContent(contentWithWhitespaces, prefix, data, suffix);
    }

    @DisplayName("Префикс содержит пробельный символ, а суффикс пустая строка")
    @Test
    void prefixShouldContainOneWhiteSpaceAndSuffixIsEmptyString() {
        String prefix = " ";
        String data = "key";
        String suffix = "";
        ContentWithWhitespaces contentWithWhitespaces = new ContentWithWhitespaces(prefix + data + suffix);
        checkContent(contentWithWhitespaces, prefix, data, suffix);
    }

    @DisplayName("Суфикс содержит пробельный символ, а префикс пустая строка")
    @Test
    void suffixShouldContainOneWhiteSpaceAndPrefixIsEmptyString() {
        String prefix = "";
        String data = "key";
        String suffix = " ";
        ContentWithWhitespaces contentWithWhitespaces = new ContentWithWhitespaces(prefix + data + suffix);
        checkContent(contentWithWhitespaces, prefix, data, suffix);
    }

    @DisplayName("Префикс и суффикс должны содержать по одному пробелу")
    @Test
    void prefixAndSuffixShouldContainOneWhiteSpaceEach() {
        String prefix = " ";
        String data = "key";
        String suffix = " ";
        ContentWithWhitespaces contentWithWhitespaces = new ContentWithWhitespaces(prefix + data + suffix);
        checkContent(contentWithWhitespaces, prefix, data, suffix);
    }

    private void checkContent(ContentWithWhitespaces contentWithWhitespaces, String expectedPrefix, String expectedData, String expectedSuffix) {
        Assertions.assertAll(
                () -> Assertions.assertEquals(expectedData, contentWithWhitespaces.getData(), "Данные" + MESSAGE),
                () -> Assertions.assertEquals(expectedPrefix, contentWithWhitespaces.getPrefix(), "Префикс" + MESSAGE),
                () -> Assertions.assertEquals(expectedSuffix, contentWithWhitespaces.getSuffix(), "Суффикс" + MESSAGE)
        );
    }
}
