package parser;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import parser.sources.Source;
import parser.sources.StringSource;

import static org.assertj.core.api.Java6Assertions.assertThat;

class DocumentSpec {

    @DisplayName("Строковое представление документа равно исходной строке из которой он парсится")
    @ParameterizedTest
    @ValueSource(strings = {"{key:\"value\"}"
            , "  {key:\"value\"}  "})
    void documentStringRepresentation(String inputJson) {
        Source source = new StringSource(inputJson);
        JsonParser parser = new JsonParser();
        Document document = parser.parse(source);
        String actual = document.toString();
        assertThat(actual).isEqualTo(inputJson);
    }
}
