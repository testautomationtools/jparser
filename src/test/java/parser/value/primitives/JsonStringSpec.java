package parser.value.primitives;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

@DisplayName("JsonString")
class JsonStringSpec {

    @DisplayName("Ковычки добавляются в строковое представление JsonString")
    @Test
    void quotesAreAddedToTheStringRepresentationOfTheString() {
        String value = "значение";
        String quote = "\"";
        JsonString jsonString = new JsonString(value);
        String actual = jsonString.toString();
        assertThat(actual)
                .isEqualTo(quote + value + quote);
    }
}
