package parser.value.containers;

import dummy.Dummy;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import parser.Key;
import parser.Quotation;
import parser.Token;

import static org.assertj.core.api.Java6Assertions.assertThat;

@DisplayName("JsonObject")
class JsonObjectSpec {

    private static Token token;

    @BeforeAll
    static void setUp() {
        token = new Token();
        token.setKey(new Key("Key", Quotation.EMPTY));
        token.setSeparator(":");
        token.setValue(new Dummy());
    }

    @DisplayName("Пустой может быть создан")
    @Nested
    class Empty {

        @DisplayName("Количество токенов равно 0")
        @Test
        void emptyObjectHasZeroChildren() {
            JsonObject json = new JsonObject();
            assertThat(json.getChildren())
                    .hasSize(0);
        }

        @DisplayName("Строковое предствление равно {}")
        @Test
        void emptyObjectStringRepresentation() {
            JsonObject json = new JsonObject();
            assertThat(json.toString())
                    .isEqualTo("{}");
        }
    }

    @DisplayName("К созданному JsonObject могут быть добавлены токены")
    @Nested
    class Tokens {

        @DisplayName("Может быть добавлен один токен")
        @Test
        void oneTokenCanBeAdded() {
            JsonObject json = new JsonObject();
            json.addToken(token);
            assertThat(json.getChildren()).hasSize(1);
        }

        @DisplayName("Могут быть добавлены два токена")
        @Test
        void twoTokensCanBeAdded() {
            JsonObject json = new JsonObject();
            json.addToken(token);
            json.addToken(token);
            assertThat(json.getChildren()).hasSize(2);
        }
    }

    @DisplayName("Строковое представление")
    @Nested
    class Representation {

        private String comma = ",";

        @DisplayName("Состоит только из фигурных скобок и самих токенов, все оставльное отсутствует")
        @Nested
        class JsonObjectWithoutExtraSeparators {
            @DisplayName("Когда состоит из одного токена")
            @Test
            void jsonOfOneToken() {
                JsonObject json = new JsonObject();
                json.addToken(token);
                String actual = json.toString();
                assertThat(actual)
                        .isEqualTo("{" + token.toString() + "}");
            }

            @DisplayName("Когда состоит из двух токенов")
            @Test
            void jsonOfTwoTokens() {
                JsonObject json = new JsonObject();
                json.addToken(token);
                json.addToken(token);
                String actual = json.toString();

                String expected = "{" + token.toString() + "," + token.toString() + "}";
                assertThat(actual)
                        .isEqualTo(expected);
            }

            @DisplayName("Когда состоит из пяти токенов")
            @Test
            void jsonOfFiveTokens() {
                JsonObject json = new JsonObject();
                json.addToken(token);
                json.addToken(token);
                json.addToken(token);
                json.addToken(token);
                json.addToken(token);
                String actual = json.toString();
                String expected = "{" + token.toString() + comma + token.toString() + comma + token.toString() + comma + token.toString() + comma + token.toString() + "}";
                assertThat(actual)
                        .isEqualTo(expected);
            }
        }

        @DisplayName("Присутствуют дополнительные пробельные символы")
        @Nested
        class JsonObjectHasWhiteSpaces {

            @DisplayName("Один токен, пробельные символы присутствуют между токеном и открывающй json { фигурной скобкой")
            @Test
            void whiteSpacesBeforeToken() {
                String prefix = "    ";
                Token token = getBasicToken();
                token.setPrefix(prefix);

                JsonObject json = new JsonObject();
                json.addToken(token);
                String actual = json.toString();
                String expected = "{" + token.toString() + "}";
                assertThat(actual)
                        .isEqualTo(expected);
            }

            @DisplayName("Один токен, есть пробельные символы после токена")
            @Test
            void whiteSpacesAfterToken() {
                String suffix = "    ";
                Token token = getBasicToken();
                token.setSuffix(suffix);

                JsonObject json = new JsonObject();
                json.addToken(token);
                String actual = json.toString();
                String expected = "{" + token.toString() + "}";
                assertThat(actual)
                        .isEqualTo(expected);
            }

            @DisplayName("Два токена, разделены запятой, до и после токенов есть пробельные символы")
            @Test
            void twoTokensWithSpaces() {
                String whiteSpaces = "  ";
                Token token1 = getBasicToken();
                token1.setPrefix(whiteSpaces);
                Token token2 = getBasicToken();
                token1.setSuffix(whiteSpaces);

                JsonObject json = new JsonObject();
                json.addToken(token1);
                json.addToken(token2);

                String actual = json.toString();
                String expected = "{" + token1.toString() + comma + token2.toString() + "}";
                assertThat(actual)
                        .isEqualTo(expected);
            }
        }
    }

    private Token getBasicToken() {
        Token token = new Token();
        token.setKey(new Key("Key", Quotation.EMPTY));
        token.setSeparator(":");
        token.setValue(new Dummy());
        return token;
    }
}
